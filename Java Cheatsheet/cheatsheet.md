# Java Cheatsheet

```java
public class Test {
    public static void main( String[] args ) {
        System.out.println("Hello World");
    }
}
```

## Input

```java
import java.io.*;

public class Test {
    public static void main( String[] args ) throws IOException {
        BufferedReader br = new BufferedReader( new InputStreamReader( System.in ));

        String s = br.readLine();
    }
}
```