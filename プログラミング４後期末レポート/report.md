<div class="cover">
    <div class="title">プログラミングⅣ</div>
    <div class="subtitle">擬似乱数</div>
    <div class="author">
        <div class="name">田村　亮弥</div>
        <div class="affiliation">４Ｓ２０</div>
    </div>
</div>

# Objective

　本課題では、コンピュータにおける乱数の発生手法である疑似乱数の概念を理解し、それのアルゴリズムをプログラムに落とし込み、実践することを目的とする。

　今回、課題に対して作成したプログラムは、HTMLとJavaScriptを用いて線形合同法による疑似乱数の発生をシミュレーションするものである。また、各要素で公式を示すことにより、可視的に乱数の変遷がわかるようにした。
　
# Usage

　本プログラムは一つのHTMLファイル（`index.html`）のみで動作する。はじめに、この`index.html`をブラウザで開く。すると、図１のような画面が表示される。
　
　![数値入力フォーム](./screenshots/image1.png)
　
　最上部に表示されているのは、線形合同法の公式である。この公式にある変数X<sub>0</sub>、A、BおよびMの値を公式以下のフォームに入力する。

　（なお、入力値のバリデーションは行わないため、正しい実数値を入力すること）

　各値を入力したら、`開始`ボタンを押す。すると、一定スパンで線形合同法の演算結果を示す。
　
# Source code

　リスト１はHTMLのソースコードである。なお、簡略化のためbodyタグの中のみ示す。
　
```html
<h1>線形合同法による疑似乱数の発生</h1>

<h2>公式</h2>
<p style="font-size: 30px; font-family: serif;"><i>X<sub>n+1</sub></i> = (<i>A</i> &times; <i>X<sub>n</sub></i> + <i>B</i>) mod <i>M</i></p>

<h2>乱数発生シミュレーター</h2>

<form id="form">
	<div class="input-group">
		<label><i>X<sub>0</sub></i> = </label>
		<input type="text" name="x0" value="8">
	</div>
	<div class="input-group">
		<label><i>A</i> = </label>
		<input type="text" name="a" value="3">
	</div>
	<div class="input-group">
		<label><i>B</i> = </label>
		<input type="text" name="b" value="5">
	</div>
	<div class="input-group">
		<label><i>M</i> = </label>
		<input type="text" name="m" value="13">
	</div>
	<button type="submit" style="display: block; margin: 25px 0 10px 80px;">開始</button>
</form>

<ul id="results">
	<li>ここに結果が表示されます（開始すると止まりません）</li>
</ul>

<script>
/*
 * リスト２
 */
</script>
```

　リスト２はJavaScriptのソースコードである。このスクリプトはリスト１のscriptタグに記述されている。
　
```javascript
var $form = document.getElementById('form');
var $results = document.getElementById('results');
$form.addEventListener('submit', function(e) {
	e.preventDefault();
	var x0 = +e.target[0].value;
	var a = +e.target[1].value;
	var b = +e.target[2].value;
	var m = +e.target[3].value;
	var results = [x0];
	setInterval(function() {
		var len = results.length;
		var result = (a * results[len - 1] + b) % m;
		results.push(result);
		
		$results.innerHTML = genHTML(results, x0, a, b, m);
		
		console.log(results);
		
		var documentH = document.body.scrollHeight;
		window.scrollTo(0, documentH);
	}, 800);
});

function genHTML(results, x0, a, b, m) {
	var html = '';
	var len = results.length;
	for(var i = 0; i < len; i++) {
		if(i === 0) {
			html += '<li><p style="font-size: 20px; font-family: serif;"><i>X<sub>0</sub></i> = ' + x0 + '</p></li>';
		} else {
			html += '<li><p style="font-size: 20px; font-family: serif;"><i>X<sub>' + i + '</sub></i> = (<i>' + a + '</i> &times; <i>X<sub>' + (i - 1) + '</sub></i> + <i>' + b + '</i>) mod <i>' + m + '</i> = <span style="font-size: 24px">' + results[i] + '</span></p></li>'
		}
	}
	return html;
}
```

# 実行結果

　本プログラムの実行結果の例を図２に示す。

　![実行結果の例](./screenshots/image2.png)

　<i>X<sub>0</sub> = 8</i>, <i>A = </i>3, <i>B = </i>5, <i>M = </i>13の場合、３回周期で同じパターンを繰り返すことが分かった。

# 考察

　今回は、コンピュータで一般的に用いられる正規分布ではなく、よりプリミティブな一様乱数を用いて乱数を表示させるプログラムを作成した。

　また、<i>X<sub>0</sub> = 3</i>, <i>A = </i>8, <i>B = </i>5, <i>M = </i>13とした場合、実行結果が常に３となた。さらに、<i>X<sub>0</sub> = 31</i>, <i>A = </i>23, <i>B = </i>87, <i>M = </i>11とした場合は、１０から０の等差数列となった。実行結果で示した数値の場合も３回周期となってしまったように、各定数の値を誤ると、十分な乱数を得ることができないことが分かった。線形合同法の周期性は、各定数を以下のようなパターンで設定した場合に発生する（Wikipedia - 線形合同法より）。

1. BとMが互いに素である
2. A-1が、Mの持つ全ての素因数で割りきれる。
3. Mが4の倍数である場合は、A-1も4の倍数である。

　つまり、各定数でこのパターンを回避することにより、ある程度不規則な乱数を得ることができる。
