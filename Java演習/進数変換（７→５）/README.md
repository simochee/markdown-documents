# Javaで進数変換

Javaで7進数の値を5進数へ変換します

[ソースコード](./Sample.java)

## コンパイル

```
$ javac Sample.java
```

## 実行結果

※ 初期値`365`の場合

```
$ java Sample
1234
```

## 注意

手計算でやるのとおんなじことをやってます。

全然スマートではありません。

また、7進数の値は`15`で固定になっています。
入力などをするときは値を変数`val`に代入してください。

## StringBufferについて

28行目〜で出てくる`StringBuffer`について

```java
// 10進数の値が5より小さくなるまで5で割り続ける
while(decimal > 0) {
    // 5で割ったあまりを先頭に挿入
    StringBuffer buf = new StringBuffer();
    buf.append(decimal % 5);
    buf.append(result);
    result = buf.toString();
    decimal /= 5;
}
```

この記述は以下のように書き換えることができます

```java
// 10進数の値が5より小さくなるまで5で割り続ける
while(decimal > 0) {
    // 5で割ったあまりを先頭に挿入
    result = (decimal % 5) + result;
    decimal /= 5;
}
```

ただし、推奨されているのは`StringBuffer`なので特に理由がなければ上の方を使ってください。