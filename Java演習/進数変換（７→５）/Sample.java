// 入力のために必要なパッケージ（？）をインポート
import java.io.*;

public class Sample {
    public static void main( String[] args ) throws IOException {

        // ７進数の初期値
        // String val = "365";

        // 初期値を入力
        BufferedReader br = new BufferedReader(new InputStreamReader( System.in) );
        System.out.print("7進数：");
        String val = br.readLine();
        
        // 文字列を文字の配列に変換
        String[] arr = val.split("");

        // 文字数を取得
        int len = arr.length;

        // 10進数の値を格納する変数
        int decimal = 0;
        
        for(int i = 0; i < len; i++) {
            // 7の(桁数-1)の累乗 * 桁の値　（手計算と一緒）
            int num = Integer.parseInt(arr[i]);
            decimal += Math.pow(7, len - i - 1) * num;
        }

        /**
         * この時点でdecimalには10進数に変換された値が入っている
         */

        // 5進数の値を格納する変数
        String result = "";

        // 10進数の値が5より小さくなるまで5で割り続ける
        while(decimal > 0) {
            // 5で割ったあまりを先頭に挿入
            StringBuffer buf = new StringBuffer();
            buf.append(decimal % 5);
            buf.append(result);
            result = buf.toString();
            decimal /= 5;
        }

        System.out.println("5進数：" + result);
    }
}